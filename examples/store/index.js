import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default (options) => new Vuex.Store({
  state: {
    borderTarget: ''
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    ...options.modules
  }
})

