/*eslint-disable*/
export default function drag(el) {
  let dragBox = el; //获取当前元素
  dragBox.ontouchstart = e => {
    document.querySelector('.time_line_wrapper').style.overflowY = 'hidden'
    //算出鼠标相对元素的位置
    let disX = e.touches[0].clientX - dragBox.offsetLeft;
    let disY = e.touches[0].clientY - dragBox.offsetTop;
    document.ontouchmove = e => {
      //用鼠标的位置减去鼠标相对元素的位置，得到元素的位置
      let left = e.touches[0].clientX - disX;
      let top = e.touches[0].clientY - disY;
      //移动当前元素
      if (left < 0) {
        left = 0
      }
      if (left > document.documentElement.clientWidth - e.target.clientWidth) {
        left = document.documentElement.clientWidth - e.target.clientWidth
      }
      if (top < 0) {
        top = 0
      }
      if (top > document.documentElement.clientHeight - e.target.clientHeight) {
        top = document.documentElement.clientHeight - e.target.clientHeight
      }
      dragBox.style.left = left + "px";
      dragBox.style.top = top + "px";
    };
    dragBox.ontouchend = e => {
      //鼠标弹起来的时候不再移动
      document.ontouchmove = null;
      //预防鼠标弹起来后还会循环（即预防鼠标放上去的时候还会移动）
      document.ontouchend = null;
      document.querySelector('.time_line_wrapper').style.overflowY = 'scroll'
    };
  };
}
