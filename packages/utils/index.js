import moment from "moment";

export function position2Time(top, date, boxHeight) {
  const startHour = Math.floor(top / 60).toString().padStart(2, '0');
  const starMin = (top % 60).toString().padEnd(2, '0');
  const startTime = (startHour + ':' + starMin);

  const ending = top + boxHeight;
  const endHour = Math.floor(ending / 60).toString().padStart(2, '0');
  const endMin = (ending % 60).toString().padEnd(2, '0');
  let endTime = (endHour + ':' + endMin);

  if (endTime === '00:00' || endTime === '24:00') endTime = '23:59'
  return {
    start: date + ' ' + startTime,
    end: date + ' ' + endTime,
    startTime,
    endTime
  };
}


export function time2Position(start, end) {
  const startDate = start;
  const endDate = end;
  const startHour = moment(startDate).hour();
  const starMin = moment(startDate).minute();

  const endHour = moment(endDate).hour();
  const endMin = moment(endDate).minute();


  const top = startHour * 60 + starMin;

  const boxHeight = endHour * 60 + endMin - top;

  return {
    top,
    boxHeight
  }
}

// 自定义取舍规则
export function customRound(num, ruleNum = 4) {
  if (parseInt(num % 1 * 10) <= ruleNum) {
    return Math.floor(num)
  } else {
    return Math.ceil(num)
  }
}


// js防抖函数
export function antiShake (timeout = 0) {
  var time
  return function(callback){
    if(time)
    {
      clearTimeout(time)
    }
    time = setTimeout(() => {
      callback && callback()
    }, timeout);
  }
}

// js节流函数
export function antiJieLiu (timeout = 0) {
  var time
  return function(callback){
    if(time)
    {
      return
    }
    time = setTimeout(() => {
      clearTimeout(time);
      time = 0
      callback && callback()
    }, timeout);
  }
}


export function getStyleVal(el, name) {
  return parseInt(document?.defaultView.getComputedStyle(el, false)[name]) || 0
}

export function usemore(num, total) {
  if (num > total) {
    return total + '+'
  } else {
    return '+' + num
  }
}

export function timeFormat (time1, time2, sp=' - ') {
  if (moment(time1).year() !== moment(time2).year()) {
    return moment(time1).format('YYYY年MM月DD日 HH:mm') + sp + moment(time2).format('YYYY年MM月DD日 HH:mm')
  } else if (moment(time1).format('YYYY-MM-DD') !== moment(time2).format('YYYY-MM-DD')) {
    return moment(time1).format('MM月DD日 HH:mm') + sp + moment(time2).format('MM月DD日 HH:mm')
  } else {
    return moment(time1).format('HH:mm') + sp + moment(time2).format('HH:mm')
  }
}

export function isIOS () {
  return navigator.userAgent.indexOf('iPhone') > -1
}

export function isHUAWEI () {
  return navigator.userAgent.indexOf('HUAWEI') > -1
}
