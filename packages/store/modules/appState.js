export default {
  namespaced: false,
  state: {
    touchDirection: 0,
    borderTarget: '',
    headerDateList: [],
    singleWidth: 0,
    timeLineScrollTop: 8 * 60,
    columnBoxScrollLeft: 0,
    currentFirstIndex: 0,
    expanded: false, //跨天日程是否展开
    singleOneDayColumnWidth: 0, //日视图单个日期宽度
    singleOneDayWidth: 0, //日视图单个日期宽度
    runTimeTop: 0,
    dragBoxHeight: 0,
    showRunTime: false,
    currentFirstOneDayDate: {}, // 日视图当前的第一个时间
    pageIndex: 0, // 控制日视图的翻页
    paddingStart: 0, // 日视图日期的月前补空
    dragFlag: false, // 日程控制点被拖拽
    listSelectDate: '' // 列表视图被选中的时间
  },
  getters: {
    getTouchDirection (state) {
      return state.touchDirection
    },
    getBorderTarget (state) {
      return state.borderTarget
    },
    getHeaderDateList (state) {
      return state.headerDateList
    },
    getSingleWidth (state) {
      return state.singleWidth
    },
    getTimeLineScrollTop (state) {
      return state.timeLineScrollTop
    },
    getColumnBoxScrollLeft (state) {
      return state.columnBoxScrollLeft
    },
    getCurrentFirstIndex (state) {
      return state.currentFirstIndex
    },
    getExpanded (state) {
      return state.expanded
    },
    getSingleOneDayWidth (state) {
      return state.singleOneDayWidth
    },
    getSingleOneDayColumnWidth (state) {
      return state.singleOneDayColumnWidth
    },
    getRunTimeTop (state) {
      return state.runTimeTop
    },
    getDragBoxHeight (state) {
      return state.dragBoxHeight
    },
    getShowRunTime (state) {
      return state.showRunTime
    },
    getCurrentFirstOneDayDate (state) {
      return state.currentFirstOneDayDate
    },
    getPageIndex (state) {
      return state.pageIndex
    },
    getPaddingStart (state) {
      return state.paddingStart
    },
    getDragFlag (state) {
      return state.dragFlag
    },
    getListSelectDate (state) {
      return state.listSelectDate
    }
  },
  mutations: {
    setTouchDirection (state, touchDirection) {
      state.touchDirection = touchDirection
    },
    setBorderTarget (state, target) {
      state.borderTarget = target
    },
    setHeaderDateList (state, dateList) {
      state.headerDateList = dateList
    },
    setSingleWidth (state, singleWidth) {
      state.singleWidth = singleWidth
    },
    setTimeLineScrollTop (state, timeLineScrollTop) {
      state.timeLineScrollTop = timeLineScrollTop
    },
    setColumnBoxScrollLeft (state, columnBoxScrollLeft) {
      state.columnBoxScrollLeft = columnBoxScrollLeft
    },
    setCurrentFirstIndex (state, currentFirstIndex) {
      state.currentFirstIndex = currentFirstIndex
    },
    setExpanded (state, expanded) {
      state.expanded = expanded
    },
    setSingleOneDayWidth (state, singleOneDayWidth) {
      state.singleOneDayWidth = singleOneDayWidth
    },
    setSingleOneDayColumnWidth (state, singleOneDayColumnWidth) {
      state.singleOneDayColumnWidth = singleOneDayColumnWidth
    },
    setRunTimeTop (state, runTimeTop) {
      state.runTimeTop = runTimeTop
    },
    setDragBoxHeight (state, dragBoxHeight) {
      state.dragBoxHeight = dragBoxHeight
    },
    setShowRunTime (state, showRunTime) {
      state.showRunTime = showRunTime
    },
    setCurrentFirstOneDayDate (state, currentFirstOneDayDate) {
      state.currentFirstOneDayDate = currentFirstOneDayDate
    },
    setPageIndex (state, pageIndex) {
      state.pageIndex = pageIndex
    },
    setPaddingStart (state, paddingStart) {
      state.paddingStart = paddingStart
    },
    setDragFlag (state, dragFlag) {
      state.dragFlag = dragFlag
    },
    setListSelectDate (state, listSelectDate) {
      state.listSelectDate = listSelectDate
    }
  },
  actions: {
    initColumnBoxScrollLeft ({commit}, scrollLeft) {
      commit('setColumnBoxScrollLeft', scrollLeft)
    }
  },
  modules: {
  }
}
